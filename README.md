
<<<<<<< HEAD


>>>ETHIC BALANCE CHANGES:
// number/number means standard/fanatic versions of the ethic respectively

>>Xenophobic:
	>Home Territory Fire Rate changed from 25%/50% to 10%/20%
>>Anthropocentrist:
	>Max Districts modifier removed and replaced with -10%/-20% Building Cost, 10%/20% Planetary Build Speed and 5%/10% Engineering Research Speed
>>Authoirtarian:
	>Monthly Influence gain decreased from 0.75/1.5 to 0.5/1
>>Elitist:
	>Added 5%/10% Specialist Pop Happiness
	>Fanatic Elitist can no longer have a democratic form of government
>>Pacifist:
	>Fanatic Pacifist Naval Capacity modifier replaced with -10% diplomatic weight
	>Added 10%/30% Defensive Fire Rate modifier
	>Added -10% Fire Rate modifier
>>Xenophile:
	>Diplomatic Influence cost changed from -20%/-40% to -40%/-80%
	>Trade Attractiveness modifier replaced with 25%/50% Immigration Attractiveness
>>Cooperative:
	>Added 2.5%/5% Worker Resource Output
>>Pluralist:
	>Leader Pool Size decreased from 3/6 to 1/2
	>Added 5%/10% Pop Happiness modifier
	>Fanatic Pluralist now has access to Equal Marketplace trade policy, giving +0.5 EC and +0.5 Unity per Trade Value
>>Spiritualist
	>Research Speed (Physics) decrease changed from -10% to -15%
	>Added 2.5/5 Planet Stability modifier



>>>CIVIC BALANCE CHANGES:

>>Feudal Society:
	>Increased Subject Power Relations Penalty from -50% to -25%
	>Added 10% Vassal Naval Capacity Contribution
	>Added 40% Subject Tax
	>Added 20% Subject Integration Influence Cost
>>Engineering Mastery:
	>Replaced Starbase Buildings and Starbase Modules modifiers with +4 Buidling Slots
	
	
	
>>>NEW CIVICS:

>>Feudal Warlords:
	>Mutually exclusive with Feudal Society
	>Requires Imperial government and some form of militarism
	>Subject Power Relations Penalty: -75%
	>Vassal Naval Capacity Contribution: +50%
	>Subject Tax: -25%
	>Subject Integration Influence Cost: +75%

>>Discriminative Society:
	>Requires Elitism or Fanatic Elitism
	>Can enslave other species
	>Monthly Unity +20%
	>Worker Resource Output +20%
	>Slave Resource Output +20%
	>Pop Happiness -10%
	>Stability -5
	>Society Research Output -10%

>>Technologically Stubborn
	>Requires to not be Materialist or Fanatic Materialist
	>Research Speed -15%
	>Resources from Jobs +10%
	>City District Upkeep -25%
	>Ship Upkeep -20%
=======
A Stellaris MP Balance mod based on the Ethics & Civics mod.
>>>>>>> 147e1aea11eb54a2fbeaf67db18062eea322a4bd
